# Starforge

Starforge is an attempt to recreate the venerable StarGen in Haskell, in order to:

  1. Create a new codebase, more up to date with new scientifical findings.
  2. Actually learn Haskell, which is something I've been trying for a while.

There is not much else to say here for now.
