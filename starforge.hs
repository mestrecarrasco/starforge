-- Bootstrap code; will be moved to it's own file any time 

import System.Random 
rnd = randomRs (0, 1) (mkStdGen 100) :: [Double]

-- Simulation functions

starforge :: [Double] -> [Planet]
starforge rs = ps
    where ps = simulation rs bs []
          bs         = [Band 5 50]

simulation :: [Double] -> [Band] -> [Planet] -> [Planet]
simulation rs [] ps = ps
simulation (r1:r2:rs) bs ps = simulation rs bs' ps'
    where
        (bs', ps') = simulationStep p bs ps
        p   = Just $ newPlanet r1 r2

simulationStep :: Maybe Planet -> [Band] -> [Planet] -> ([Band], [Planet])
simulationStep Nothing bs ps = (bs, ps)
simulationStep (Just p) bs ps = simulationStep p'' bs' ps'
    where (p',  bs')  = aggregate (p, bs)
          (p'', ps', _) = coalesce (Just p', ps, [])

avgCloudEccentricity :: Double
avgCloudEccentricity = 0.25

-- Aggregation functions

aggregate :: (Planet, [Band]) -> (Planet, [Band])
aggregate (Planet m o, bs) =
    if mi > threshold
    then aggregate ((Planet m' o), bs')
    else ((Planet m' o), bs')
    where threshold = 0.0001
          (as, bs') = aggregateMultiple (Planet m o) bs
          mi        = aggregatedMass (Planet m o) as
          m'        = m + mi

aggregateMultiple :: Planet -> [Band] -> ([Band], [Band])
aggregateMultiple _ [] = ([], [])
aggregateMultiple p (b:bs) = (a ++ as, n ++ ns)
    where (a, n)   = aggregateSingle p b
          (as, ns) = aggregateMultiple p bs

aggregateSingle :: Planet -> Band -> ([Band], [Band])
aggregateSingle p (Band bi bo)
    | (bo <  ri) || (ro <  bi) = ([], [Band bi bo])
    | (ri <= bi) && (bo <= ro) = ([Band bi bo], [])
    | (bi <= ri) && (bo <= ro) = ([Band ri bo], [Band bi ri])
    | (ri <= bi) && (ro <= bo) = ([Band bi ro], [Band ro bo])
    | (bi <  ri) && (ro <  bo) = ([Band ri ro], [Band bi ri, Band ro bo])
    where ri = innerReach p
          ro = outerReach p

aggregatedMass :: Planet -> [Band] -> Double
aggregatedMass p [] = 0
aggregatedMass p (b:bs) = v * d + aggregatedMass p bs
    where v = approximateBandVolume p b
          d = aggregatedDensity p

sweptVolume :: Planet -> Double
sweptVolume (Planet m (Ellipse a e)) = 2 * pi * bw * (xa + xp)
    where bw = bandwidth (Planet m (Ellipse a e))
          ra = aphelion (Ellipse a e)
          rp = perihelion (Ellipse a e)
          xa = gravitationalReach ra m
          xp = gravitationalReach rp m

dustDensity :: Double -> Double
dustDensity r = a * exp (-((alpha * r) ** (1/n)))
    where a     = 1.5e-3 -- solar masses per cubic A.U. (Dole 1969 p. 14)
          alpha = 5
          n     = 3

aggregatedDensity :: Planet -> Double
aggregatedDensity (Planet m (Ellipse a e)) = (k * rho) / (1 + (k - 1)*(mc / m)**0.5)
    where k   = 50
          rho = dustDensity a
          mc  = criticalMass (Ellipse a e)

tanTheta :: Planet -> Double
tanTheta (Planet m (Ellipse a e)) = (xa-xp)/(or-ir)
    where xa = aphelion (Ellipse a e)
          xp = perihelion (Ellipse a e)
          or = outerReach (Planet m (Ellipse a e))
          ir = innerReach (Planet m (Ellipse a e))

approximateBandSectionArea :: Planet -> Band -> Double
approximateBandSectionArea (Planet m o) (Band bi bo)= w * (bl + bs)
    where w  = bo-bi
          bs = xp + tt * (bi-ir)
          bl = xa + tt * (or-bo)
          tt = tanTheta (Planet m o)
          ir = innerReach (Planet m o)
          or = outerReach (Planet m o)
          xa = aphelion o
          xp = perihelion o

-- I'm definitely not sure about this. Why the height of this 
-- solid is just `2 * pi`? This completely disregards the 
-- distance to the center of mass. Still, this is what is 
-- stated at DOLE 1969, p. 20
approximateBandVolume :: Planet -> Band -> Double
approximateBandVolume p b = 2 * pi * a
    where a = approximateBandSectionArea p b

-- Coalescing functions

coalesce :: (Maybe Planet,  [Planet], [Planet]) -> (Maybe Planet, [Planet], [Planet])
coalesce (Nothing, [], as) = (Nothing, [], as)
coalesce (Just np, [], as) = (Nothing, [np], as)
coalesce (Nothing, ps, as) = (Nothing, (reverse as) ++ ps, [])
coalesce (Just np, p:ps, as) = 
    if withinCoalescingRange np p 
    then (Just $ coalesceSingle np p, (reverse as) ++ ps, [])
    else coalesce (Just np, ps, p:as)

aphelionCoalescingDistance :: Planet -> Double
aphelionCoalescingDistance (Planet m (Ellipse a e)) = ra * (1 + (mu m)**1/4) - a
    where ra = aphelion (Ellipse a e)

perihelionCoalescingDistance :: Planet -> Double
perihelionCoalescingDistance (Planet m (Ellipse a e)) = a - rp * (1 - (mu m)**1/4)
    where rp = perihelion (Ellipse a e)

coalescingRange :: Planet -> Planet -> (Double, Double)
coalescingRange (Planet m1 (Ellipse a1 e1)) (Planet m2 (Ellipse a2 e2)) 
    | a2 >= a1  = (d1a, d2a)
    | otherwise = (d1b, d2b)
    where d1a = aphelionCoalescingDistance (Planet m1 (Ellipse a1 e1))
          d2a = perihelionCoalescingDistance (Planet m2 (Ellipse a2 e2))
          d1b = perihelionCoalescingDistance (Planet m1 (Ellipse a1 e1))
          d2b = aphelionCoalescingDistance (Planet m2 (Ellipse a2 e2))

withinCoalescingRange :: Planet -> Planet -> Bool
withinCoalescingRange (Planet m1 (Ellipse a1 e1)) (Planet m2 (Ellipse a2 e2)) = d' <= d1' || d' <= d2'
    where d        = a2 - a1
          d'       = abs d
          (d1, d2) = coalescingRange (Planet m1 (Ellipse a1 e1)) (Planet m2 (Ellipse a2 e2))
          d1'      = abs d1
          d2'      = abs d2

coalesceSingle :: Planet -> Planet -> Planet
coalesceSingle (Planet m1 (Ellipse a1 e1)) (Planet m2 (Ellipse a2 e2)) = Planet m3 (Ellipse a3 e3)
    where m3 = m1 + m3
          a3 = (m1 + m2) / (m1/a1 + m2/a2)
          e3 = (1 - y ** 2) ** 1/2
          y  = (x1 + x2) / ((m1 + m2) * a3 ** 1/2)
          x1 = eccTerm m1 a1 e1
          x2 = eccTerm m2 a2 e2
          eccTerm m a e = (m * a ** 1/2) * (1 - e ** 2)**1/2

-- Planet

data Planet = Planet {
    mass :: Double,
    orbit :: Ellipse
} deriving (Show)

planetStartingMass :: Double
planetStartingMass = 1e-15 -- solar masses

planetStartingSemiMajorAxis :: Double -> Double
planetStartingSemiMajorAxis y = 50.0 * y

planetStartingEccentricity :: Double -> Double
planetStartingEccentricity y = 1-(1-y)**q
    where q = 0.077 :: Double

newPlanet :: Double -> Double -> Planet
newPlanet yi yj = Planet m (Ellipse a e)
    where a = planetStartingSemiMajorAxis yi
          e = planetStartingEccentricity  yj
          m = planetStartingMass

mu :: Double -> Double
mu m = m / (1+m)

gravitationalReach :: Double -> Double -> Double
gravitationalReach r m = r * (mu m)**(1/4)

innerReach :: Planet -> Double
innerReach (Planet m (Ellipse a e)) = a -a*e -xp -w * (rp-xp) / (1+w)
    where xp = gravitationalReach rp m
          rp = perihelion (Ellipse a e)
          w  = avgCloudEccentricity

outerReach :: Planet -> Double
outerReach (Planet m (Ellipse a e)) = a +a*e +xa +w * (ra-xa) / (1-w)
    where xa = gravitationalReach ra m
          ra = aphelion (Ellipse a e)
          w  = avgCloudEccentricity

bandwidth :: Planet -> Double
bandwidth p = op - ip
    where ip = innerReach p
          op = outerReach p

-- Ellipse 

data Ellipse = Ellipse {
    semiMajorAxis :: Double,
    eccentricity :: Double
} deriving (Show)

perihelion :: Ellipse -> Double
perihelion (Ellipse a e) = a*(1-e)

aphelion :: Ellipse -> Double
aphelion (Ellipse a e) = a*(1+e)

criticalMass :: Ellipse -> Double
criticalMass orbit = b * rp **(-3/4)
    where b  = 1.2e-5
          rp = perihelion orbit

-- Band 

data Band = Band Double Double deriving (Show)
